import React from 'react';
import { Home } from './components/pages/Home';
import { About } from './components/pages/About';
import { Contact } from './components/pages/Contact';
import { ReactStarter } from './components/pages/ReactStarter';
import { Products } from './components/pages/Products';

export const routes = {
    '/': () => <Home />,
    '/about': () => <About />,
    '/contact': () => <Contact />,
    '/learn': () => <ReactStarter />,
    '/products*': () => <Products />
    // "/products/:id": ({ id }: { id: number }) => <Products id={id} />
}