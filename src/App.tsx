import React from 'react';
import { Navbar } from './components/Navbar';
import { routes } from './routes';
import { useRoutes } from 'hookrouter';


export const App: React.FC = () => {
  const match = useRoutes(routes);
  return(
    <>
    <Navbar />
    <div className="content">
    {match}
    </div>
    </>
  )
}

