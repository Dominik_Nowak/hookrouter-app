import React from 'react'
import { A, useRoutes } from 'hookrouter'
import { Back } from '../../Back';

const routes = {
    '/1': () => 'Trust us, this is a very nice product!',
    '/2': () => 'This is also a good stuff',
    '/3': () => 'This one not that much'
};

export const Products: React.FC = () => {
    const routeResult = useRoutes(routes);

    return (
        <div>
            <ul> Our products:
                <li><A href="1">Product 1</A></li>
                <li><A href="2">Product 2</A></li>
                <li><A href="3">Product 3</A></li>
            </ul>
            <p>{routeResult}</p>
            <Back />
        </div>
    );
}