import React from 'react';

export const Back: React.FC = () => {
    return (
        <button onClick={()=>window.history.back()}>Back</button>
    )
}