import React from 'react';
import './index.css';
import { Navlink } from '../Navlink';

export const Navbar: React.FC = () => {

  return (
      <nav className="site-nav">
        <button className="side-menu-trigger">Menu</button>
        <aside className="side-menu">
          <ul>
            <li>
              <Navlink href="/" content="Home" />
            </li>
            <li>
              <Navlink href="/about" content="About us" />
            </li>
            <li>
              <Navlink href="/products/" content="Products" />
              <ul>
                <li>
                  <Navlink href="/products/1" content="Product 1" />
                </li>
                <li>
                  <Navlink href="/products/2" content="Product 2" />
                </li>
                <li>
                  <Navlink href="/products/3" content="Product 3" />
                </li>
              </ul>
            </li>
            <li>
              <Navlink href="/contact" content="Contact" />
            </li>
            <li>
              <Navlink href="/learn" content="Learn React" />
            </li>
          </ul>
        </aside>
      </nav>
  )
};