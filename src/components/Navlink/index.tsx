import React from 'react'
import { A, usePath } from 'hookrouter'

interface NavlinkParams {
    href: string;
    content: string;
}

export const Navlink: React.FC<NavlinkParams> = ({ href, content }) => {
    const path = usePath();

    return (
        <A href={href} style={path === href ? { backgroundColor: 'lightgreen' } : {}}>{content}</A>
    )
} 